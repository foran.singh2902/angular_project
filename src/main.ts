import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

import { AppModule } from "./app/app.module";
import { environment } from "./environments/environment";

if (environment.production) {
  enableProdMode();
}
// this is the root module as its bootstrapped to be loaded when application is loaded
platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
// this will result in loading of app.component.html
