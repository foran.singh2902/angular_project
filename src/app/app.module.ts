import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { FormsModule } from "@angular/forms";
import { WelcomeComponent } from "./welcome/welcome.component";
import { ErrorComponent } from "./error/error.component";
import { ListTodosComponent } from "./list-todos/list-todos.component";
import { MenuComponent } from "./menu/menu.component";
import { FooterComponent } from "./footer/footer.component";
import { LogoutComponent } from "./logout/logout.component";
import { HttpClientModule } from "@angular/common/http";
import { SummaryPipe } from "./custom-pipes/summary.pipe";
import { TemplateFormsComponent } from "./template-forms/template-forms.component";
import { HttpCallComponent } from "./http-call/http-call.component";
import { PipesComponent } from "./pipes/pipes.component";
import { RestApiService } from "./service/rest-api.service";
import { HttpModule } from "@angular/http";
import { ItunesService } from "./service/itunes.service";
import { ItunesComponent } from './itunes/itunes.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WelcomeComponent,
    ErrorComponent,
    ListTodosComponent,
    MenuComponent,
    FooterComponent,
    LogoutComponent,
    SummaryPipe,
    TemplateFormsComponent,
    HttpCallComponent,
    PipesComponent,
    ItunesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    FormsModule,
    HttpModule
  ],
  providers: [RestApiService, ItunesService],
  bootstrap: [AppComponent]
  // bootstrap indicates which component to load when application loads
})
export class AppModule {}
/*
angular modules are those which carry @NgModules
javascript modules are any files carrying .ts and .js extensions
@NgModule is a grouping of a number of angular building blocks
*/
