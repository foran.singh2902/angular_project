import { Pipe, PipeTransform } from "@angular/core";
// PipeTransform is an interface responsible for the shape of pipes
@Pipe({
  name: "summary"
})
// name of the pipe, that can be used to call this custom pipe
export class SummaryPipe implements PipeTransform {
  transform(value: string, limit: number) {
    if (!value) {
      return null;
    }
    let actualLimit = limit ? limit : 50;
    return value.substr(0, actualLimit) + "....";
  }
}
