import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-pipes",
  templateUrl: "./pipes.component.html",
  styleUrls: ["./pipes.component.css"]
})
export class PipesComponent implements OnInit {
  constructor() {}
  longText =
    "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Autem molestias recusandae nulla odio necessitatibus alias ut id iure voluptate accusantium architecto quasi minus, non magni blanditiis consequatur saepe eaque exercitationem.";
  course = {
    title: "Learn Angular",
    rating: 3.4566,
    students: 345234,
    price: 2300,
    release: new Date(2020, 3, 1)
  };
  ngOnInit() {}
}
