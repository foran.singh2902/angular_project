export interface User {
  // creating a model class to receive the data over http service
  // can also use class instead of inteface but prefer interface
  id: string;
  name: string;
  username: string;
  email: string;
}
