import { Component, OnInit } from "@angular/core";
import { ItunesService } from "../service/itunes.service";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-itunes",
  templateUrl: "./itunes.component.html",
  styleUrls: ["./itunes.component.css"]
})
export class ItunesComponent implements OnInit {
  search: string = "";
  constructor(public itunesService: ItunesService) {}
  public results;
  onSearch(form: NgForm) {
    console.log("value in form ", form.value);
    this.itunesService.search(form.value.search);
    this.results = this.itunesService.results;
    console.log("results :", this.itunesService.results);
  }
  doSearch() {}
  ngOnInit() {
    //this.doSearch();
  }
}
