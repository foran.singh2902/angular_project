import { Injectable } from "@angular/core";
// @Injectable makes this injectable ready
@Injectable({
  providedIn: "root"
})
export class HardcodedAuthenticationService {
  constructor() {}
  authenticate(username: string, password: string) {
    if (username === "fsingh" && password === "admin") {
      sessionStorage.setItem("authenticatedUser", username);
      return true;
    }
    return false;
  }
  isUserLoggedIn(): boolean {
    let user = sessionStorage.getItem("authenticatedUser");
    return !(user === null);
  }
  logout() {
    sessionStorage.removeItem("authenticatedUser");
  }
}
