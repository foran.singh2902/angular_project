import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import {
  HttpClient,
  HttpHeaders,
  HttpClientModule,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";

import { environment } from "src/environments/environment";
import { Observable, throwError } from "rxjs";
import { WorkTodo } from "../models/todo.model";
import { map } from "rxjs/operators";
import { catchError } from "rxjs/operators";

const API_URL = environment.apiUrl;
@Injectable({
  providedIn: "root"
})
export class RestApiService {
  constructor(private _http: Http) {}

  // API: GET /todos
  public getAllTodos(): Observable<WorkTodo[]> {
    console.log("inside rest-api.service");
    return this._http.get(API_URL + "/worktodos").pipe(
      map(response => {
        const todos = response.json();
        return todos.map(todo => new WorkTodo(todo));
      }),
      catchError(this.handleError)
    );
  }

  public getTodoById(todoId: number): Observable<WorkTodo> {
    return this._http.get(API_URL + "/worktodos/" + todoId).pipe(
      map(response => {
        return new WorkTodo(response.json());
      }),
      catchError(this.handleError)
    );
  }

  public createTodo(todo: WorkTodo): Observable<WorkTodo> {
    console.log("inside the add method", todo.id);
    todo.id = <number>todo.id;
    console.log("updated todo ", todo.id);
    return this._http.post(API_URL + "/worktodos", todo).pipe(
      map(response => {
        return new WorkTodo(response.json());
      }),
      catchError(this.handleError)
    );
  }

  public updateTodo(todo: WorkTodo): Observable<WorkTodo> {
    return this._http.put(API_URL + "/worktodos/" + todo.id, todo).pipe(
      map(response => {
        return new WorkTodo(response.json());
      }),
      catchError(this.handleError)
    );
  }

  public deleteTodoById(todoId: number): Observable<null> {
    return this._http.delete(API_URL + "/worktodos/" + todoId).pipe(
      map(response => null),
      catchError(this.handleError)
    );
  }

  private handleError(error: Response | any) {
    console.error("ApiService::handleError", error);
    console.log("this is an error");
    return Observable.throw(error);
  }
}
