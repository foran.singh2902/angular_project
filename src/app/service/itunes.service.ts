import { Injectable } from "@angular/core";
import { HttpModule, Http } from "@angular/http";
//import "rxjs/add/operator/toPromise"; no need tom import as
// As of rxjs 5.5.0-beta.5+ the toPromise method is now a permanent method of Observable.
// You don't need to import it anymore

@Injectable({
  providedIn: "root"
})
export class ItunesService {
  apiRoot: string = `https://itunes.apple.com/search`;
  results: Object = [];
  loading: boolean;
  constructor(private http: Http) {}
  search(term: string) {
    let promise = new Promise((resolve, reject) => {
      let apiUrl = `${this.apiRoot}?term=${term}&media=music&limit=20`;
      this.http
        .get(apiUrl)
        .toPromise()
        .then(
          res => {
            this.results = res.json().results;
            console.log("results in servie ", this.results);
            resolve();
          },
          msg => {
            reject();
          }
        );
    });
    return promise;
  }
}
