import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";
import { User } from "../models/user.model";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class DataService {
  apiUrl: string = "https://jsonplaceholder.typicode.com/users";
  constructor(private _http: HttpClient) {}
  getUsers(): Observable<User[]> {
    return this._http.get<User[]>(this.apiUrl).pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError("server error: unable to fetch the data");
        // or can use error.message
      })
    );
  }
}
