import { Component, OnInit } from "@angular/core";
import { WorkTodo } from "../models/todo.model";
import { RestApiService } from "../service/rest-api.service";
import { Observable } from "rxjs";
import { NgForm } from "@angular/forms";

export class Todo {
  constructor(
    public idty: number,
    public description: string,
    public done: boolean,
    public date: Date
  ) {}
}
@Component({
  selector: "app-list-todos",
  templateUrl: "./list-todos.component.html",
  styleUrls: ["./list-todos.component.css"]
})
export class ListTodosComponent implements OnInit {
  todos = [
    new Todo(1, "Learn Angular", true, new Date()),
    new Todo(2, "Learn Spring", false, new Date()),
    new Todo(3, "Make POC", false, new Date())
  ];
  longText =
    "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Autem molestias recusandae nulla odio necessitatibus alias ut id iure voluptate accusantium architecto quasi minus, non magni blanditiis consequatur saepe eaque exercitationem.";
  course = {
    title: "Learn Angular",
    rating: 3.4566,
    students: 345234,
    price: 2300,
    release: new Date(2020, 3, 1)
  };
  // todo = {
  //   id: 1,
  //   description: "Learn to code angular"
  // };
  constructor(private api: RestApiService) {}
  worktodos: WorkTodo[] = [];
  id: number = 0;
  title: string = "";
  complete: boolean = true;
  titleUpdate: string = "";
  completeUpdate: boolean = true;
  workObj: WorkTodo = {
    id: this.id,
    title: this.title,
    complete: this.complete
  };
  demo: WorkTodo = { id: 4, title: "Lunch at home", complete: true };
  todoId: number = 0;
  update: WorkTodo;

  // Simulate GET /todos
  // getAllTodos(): Observable<WorkTodo[]> {
  //   return this.api.getAllTodos();
  // }

  // Simulate GET /todos/:id
  getTodoById(id: number): WorkTodo {
    return this.worktodos.filter(worktodo => worktodo.id === id).pop();
  }
  onSubmit(form: NgForm) {
    console.log("id :", this.id, "title :", this.title);
    this.onAddTodo({
      id: this.id,
      title: this.title,
      complete: this.complete
    });
  }
  onUpdateClick(work) {
    this.update = work;
    console.log("value of update is :", this.update);
  }
  onUpdate(form: NgForm) {
    console.log("value of form :", form.value);
    let updatedData: WorkTodo = {
      id: this.update.id,
      title: form.value.title,
      complete: form.value.complete
    };
    this.onUpdateTodo(updatedData);
    this.getAllToDos();
  }
  onAddTodo(worktodo: WorkTodo) {
    console.log(
      "inside list-todos onAddTodo",
      worktodo.id,
      worktodo.title,
      this.title
    );
    worktodo.id = <number>this.id;
    worktodo.title = this.title;
    worktodo.complete = <boolean>this.complete;
    console.log("id is :", worktodo.id);
    this.api.createTodo(worktodo).subscribe(newTodo => {
      this.worktodos = this.worktodos.concat(newTodo);
    });
  }

  onRemoveTodo(todo: WorkTodo) {
    console.log("inside delete :", todo);
    this.api.deleteTodoById(todo.id).subscribe(_ => {
      this.worktodos = this.worktodos.filter(t => t.id !== todo.id);
    });
  }

  onUpdateTodo(todo: WorkTodo) {
    this.api.updateTodo(todo).subscribe(updatedTodo => {
      todo = updatedTodo;
    });
    console.log("inside the onUpdateTodo :", todo);
    this.getAllToDos();
  }
  getAllToDos() {
    this.api.getAllTodos().subscribe(worktodos => {
      this.worktodos = worktodos;
    });
    console.log("length of :", this.worktodos);
    //this.todoId = this.worktodos[this.worktodos.length - 1].id + 1;
  }
  ngOnInit() {
    //this.onAddTodo(this.demo);
    // this.onRemoveTodo(this.demo);
    //this.onUpdateTodo(this.demo);
    this.getAllToDos();
    //console.log("inside init of list-todos.components.ts", this.worktodos);
  }
}
