import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-template-forms",
  templateUrl: "./template-forms.component.html",
  styleUrls: ["./template-forms.component.css"],
})
export class TemplateFormsComponent implements OnInit {
  username: string = "";
  password: string = "";
  invalidLogin: boolean = false;
  errorMessage: string = "Invalid Credentials";

  // Router component
  // dependency injection is a built in feature of angular
  constructor() {}

  ngOnInit() {}
  handleLogin() {
    // if (this.username === "fsingh" && this.password == "admin") {
    // redirect to welcome page

    this.invalidLogin = true;

    console.log(this.username);
  }
}
