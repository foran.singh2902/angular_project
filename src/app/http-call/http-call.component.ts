import { Component, OnInit } from "@angular/core";
import { DataService } from "../service/data.service";
import { User } from "../models/user.model";

@Component({
  selector: "app-http-call",
  templateUrl: "./http-call.component.html",
  styleUrls: ["./http-call.component.css"]
})
export class HttpCallComponent implements OnInit {
  errorMsg = "";
  users: User[];
  constructor(private dataService: DataService) {}

  ngOnInit() {
    return this.dataService.getUsers().subscribe(
      data => (this.users = data),
      err => (this.errorMsg = err)
    );
  }
}
