import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
// we need to import the components added in 'Routes=[]' so as to use them
import { LoginComponent } from "./login/login.component";
import { WelcomeComponent } from "./welcome/welcome.component";
import { ErrorComponent } from "./error/error.component";
import { ListTodosComponent } from "./list-todos/list-todos.component";
import { LogoutComponent } from "./logout/logout.component";
import { HttpCallComponent } from "./http-call/http-call.component";
import { PipesComponent } from "./pipes/pipes.component";
import { TemplateFormsComponent } from "./template-forms/template-forms.component";
import { ItunesComponent } from "./itunes/itunes.component";
// we get this file because during the creation of project we selected yes for routing
// Here we handle routind to different components
const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: "login", component: LoginComponent },
  { path: "welcome/:name", component: WelcomeComponent },
  // remember to place the path for anything only to last else
  // it will also start picking up other routes
  { path: "todos", component: ListTodosComponent },
  { path: "logout", component: LogoutComponent },
  { path: "httpcall", component: HttpCallComponent },
  { path: "pipes", component: PipesComponent },
  { path: "templateforms", component: TemplateFormsComponent },
  { path: "itunes", component: ItunesComponent },
  { path: "**", component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
