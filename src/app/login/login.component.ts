import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { HardcodedAuthenticationService } from "../service/hardcoded-authentication.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  username: string = "";
  password: string = "";
  invalidLogin: boolean = false;
  errorMessage: string = "Invalid Credentials";

  // Router component
  // dependency injection is a built in feature of angular
  constructor(
    private router: Router,
    private auth: HardcodedAuthenticationService
  ) {}

  ngOnInit() {}
  handleLogin() {
    // if (this.username === "fsingh" && this.password == "admin") {
    // redirect to welcome page
    if (this.auth.authenticate(this.username, this.password)) {
      this.router.navigate(["welcome", this.username]);
      // passing the params now, as welcome page route requires a name
      this.invalidLogin = false;
    } else {
      this.invalidLogin = true;
    }
    console.log(this.username);
  }
}
